package pages;

import net.serenitybdd.core.pages.PageObject;
import org.apache.commons.codec.binary.Base64;


public class CorePage extends PageObject {

    public void encrypt(String string){
        byte[] encodedBytes = Base64.encodeBase64(string.getBytes());
        System.out.println("encodedBytes "+ new String(encodedBytes));
    }

    public void decrypt(String encodedBytes){
        byte[] decodedBytes = Base64.decodeBase64(encodedBytes);
        System.out.println("decodedBytes "+ new String(decodedBytes));
    }




}
