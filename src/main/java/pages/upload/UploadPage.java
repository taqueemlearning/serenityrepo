package pages.upload;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.support.FindBy;
import pages.CorePage;
import utils.PropKeys;
import utils.constants.LabelTexts;

public class UploadPage extends CorePage {


    @FindBy(css = "input#uploadfile_0")
    private WebElementFacade inputFileUpload;

    @FindBy(css = "input#terms")
    private WebElementFacade checkBoxTerms;

    @FindBy(css = "button#submitbutton")
    private WebElementFacade buttonSubmitFile;

    @FindBy(css = "h3#res>center")
    private WebElementFacade textMessage;

    protected void navigateToHomepage() {
        openUrl(System.getProperty(PropKeys.uploadURL));
    }

    protected void uploadFile(String path) {
        inputFileUpload.sendKeys(path);
        clickOn(checkBoxTerms);
    }

    protected void submitFile() {
        clickOn(buttonSubmitFile);
    }

    protected void verifySuccessSendMessage() {
        System.out.println(textMessage.getText());
        Assert.assertTrue("The success message is not displayed correctly",
                textMessage.getText().contains(LabelTexts.textSuccessMessage));
    }

    public void verifyErrorMessage(String errorMessage) {
        Assert.assertTrue("The failure message is not displayed ",
                textMessage.getText().contains(errorMessage));
    }


}
