package pages.openCart;

import pages.CorePage;
import utils.PropKeys;


public class HomePage extends CorePage {

    public void navigateToOpenCartPage() {
        openUrl(System.getProperty(PropKeys.openCartURL));
    }

}
