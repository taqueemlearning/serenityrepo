package pages.openCart;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CorePage;
import utils.SessionVariables;

import java.util.List;
import java.util.regex.Pattern;

import static utils.constants.LabelTexts.textNoProductMatch;

public class SearchPage extends CorePage {

    @FindBy(css = "input[name='search']")
    private WebElementFacade inputSearch;

    @FindBy(css = "#search button")
    private WebElementFacade buttonSearch;

    @FindBy(css = ".caption a")
    private List<WebElement> listLinkTextItemName;

    @FindBy(xpath = "(//div[@id='content']/p)[2]")
    private WebElementFacade textNoProductMatching;


    protected void searchItem() {
        clickOn(buttonSearch);
    }

    protected void searchItem(String itemName) {
        typeInto(inputSearch, itemName);
        clickOn(buttonSearch);
    }

    protected void verifyResultTitle(String expItemName) {
        if (listLinkTextItemName.size() >= 1) {
            for (WebElement element : listLinkTextItemName) {
                String actItemName = element.getText();
                Assert.assertTrue("The full name " + actItemName + " does not contain the expected " +
                        expItemName, Pattern.compile(Pattern.quote(expItemName), Pattern.CASE_INSENSITIVE)
                        .matcher(actItemName).find());
            }
        } else {
            SessionVariables.getScenario().log("Please Check the search criteria " + expItemName);
            Assert.fail("Please correct the search criteria as no result is shown");
        }
    }

    protected void verifyNoProductMessage() {
        waitFor(textNoProductMatching);
        Assert.assertTrue("The no product message is not displayed correctly",
                textNoProductMatching.getText().equalsIgnoreCase(textNoProductMatch));

    }

    protected void verifyProductList() {
        Assert.assertTrue("No Products displayed",
                listLinkTextItemName.size() >= 1);

    }

}
