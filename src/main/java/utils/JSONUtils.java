package utils;


import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class JSONUtils {


    public static JSONObject getJSONObj(String filepath) {
        InputStream is;
        String jsonString = "";
        try {
            is = new FileInputStream(getFile(filepath));
            jsonString = IOUtils.toString(is, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(jsonString);


    }


    public static File getFile(String path) {
        File f = new File(path);
        if (!f.exists())
            Assert.fail("There is no file at the location " + path);
        return f;
    }
}
