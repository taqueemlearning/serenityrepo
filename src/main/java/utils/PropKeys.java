package utils;

public interface PropKeys {

    String uploadURL = "uploadURL";
    String openCartURL = "openCartURL";
    String bookingURI = "bookingURI";
    String adminUser = "adminUser";
    String adminPassword = "adminPassword";
}
