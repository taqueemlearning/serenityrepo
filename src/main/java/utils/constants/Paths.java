package utils.constants;

public interface Paths {

    String envProperty="src/test/resources/env.properties";

    String pathToInvalidSizeFile="";

    String pathPayloadCreateBooking="src/main/resources/testData/payloads/booking/createBooking.json";
}
