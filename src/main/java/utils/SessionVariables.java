package utils;


import io.cucumber.java.Scenario;
import lombok.Data;

@Data
public class SessionVariables {

    private static Scenario scenario;

    public static Scenario getScenario() {
        return scenario;
    }

    public static void setScenario(Scenario scenario) {
        SessionVariables.scenario = scenario;
    }
}
