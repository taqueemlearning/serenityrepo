package api;

import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import utils.JSONUtils;
import utils.PropKeys;
import utils.SessionVariables;
import utils.constants.Paths;


import java.io.File;

public class BookingAPI {
    RequestSpecification request;
    Response response;

    public RequestSpecification getRequestSpecification() {
        RestAssured.baseURI = System.getProperty(PropKeys.bookingURI);
        request = SerenityRest.given();
        return request;
    }


    public Response getBookingDetails(String id) {
        response = request
                .when()
                .get("booking/" + id);
        return response;
    }

    protected void verifyStatusCode(int statusCode) {
        SessionVariables.getScenario().log(response.then().log().body().toString());
        response
                .then()
                .assertThat()
                .statusCode(statusCode);
    }

    protected Response enterTicketDetails(DataTable dataTable) {
        File payload = new File(Paths.pathPayloadCreateBooking);
        response = request.contentType(ContentType.JSON)
                .body(payload)
                .when()
                .post("booking");

        return response;
    }

    protected Response modifyRequestParams(String parameter, String value, String id) {

        JSONObject jsonObject = JSONUtils.getJSONObj(Paths.pathPayloadCreateBooking);
        jsonObject.put(parameter,value);
        System.out.println(jsonObject);
        response = request.contentType(ContentType.JSON)
                .body(jsonObject.toString())
                .when()
                .put("booking/" + id);

        return response;
    }

    protected void authorize() {
        byte[] decodedBytesUser = Base64.decodeBase64(System.getProperty(PropKeys.adminUser));
        byte[] decodedBytesPass = Base64.decodeBase64(System.getProperty(PropKeys.adminPassword));
        request = request
                .auth()
                .preemptive()
                .basic(new String(decodedBytesUser), new String(decodedBytesPass));
    }

}
