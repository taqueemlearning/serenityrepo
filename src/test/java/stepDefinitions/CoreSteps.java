package stepDefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utils.SessionVariables;
import utils.constants.Paths;
import utils.fileReaders.PropertyReader;

public class CoreSteps {


    @Before
    public void beforeEachScenario(Scenario scenario){
        PropertyReader.setProperty(Paths.envProperty);
        SessionVariables.setScenario(scenario);
    }
}
