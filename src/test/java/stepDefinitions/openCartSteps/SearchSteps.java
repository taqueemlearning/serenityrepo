package stepDefinitions.openCartSteps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.openCart.SearchPage;

public class SearchSteps extends SearchPage {

    @When("he looks up {string}")
    public void he_looks_up(String itemName) {
        searchItem(itemName);
    }

    @Then("he should see the results about {string}")
    public void he_should_see_the_results_about(String itemName) {
        verifyResultTitle(itemName);
    }

    @Then("he should not see any products")
    public void he_should_not_see_any_products() {
        verifyNoProductMessage();
    }

    @When("he search without any input")
    public void he_search_without_any_input() {
        searchItem();
    }

    @Then("he should see the results")
    public void he_should_see_the_results() {
        verifyProductList();
    }


}
