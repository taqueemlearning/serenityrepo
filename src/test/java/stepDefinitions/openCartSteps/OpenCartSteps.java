package stepDefinitions.openCartSteps;

import io.cucumber.java.en.Given;
import pages.openCart.HomePage;

public class OpenCartSteps extends HomePage {

    @Given("User is on the open Cart Homepage")
    public void user_is_on_the_open_cart_homepage() {
        navigateToOpenCartPage();
    }
}
