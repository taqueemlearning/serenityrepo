package stepDefinitions.apiSteps;

import api.BookingAPI;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BookingAPISteps extends BookingAPI {

    @Given("I am on the ticket platform")
    public void i_am_on_the_ticket_platform() {
        getRequestSpecification();
    }

    @When("I enter the ticket id as {string}")
    public void i_enter_the_ticket_id_as(String id) {
        getBookingDetails(id);
    }

    @Then("I should see the ticket details")
    public void i_should_see_the_ticket_details() {
        verifyStatusCode(200);
    }

    @When("I enter the ticket details")
    public void i_enter_the_ticket_details(DataTable dataTable) {
        enterTicketDetails(dataTable);
    }


    @Then("I should be able to book a ticket")
    public void i_should_be_able_to_book_a_ticket() {
        verifyStatusCode(200);
    }

    @Then("I should get error {string}")
    public void i_should_get_error(String statusCode) {
        verifyStatusCode(Integer.parseInt(statusCode));
    }

    @When("I modify the {string} as {string}")
    public void i_modify_the_as(String parameter, String value) {


    }

    @When("I modify the {string} as {string} for the Booking id {string}")
    public void i_modify_the_as_for_the_booking_id(String parameter, String value, String id) {
        modifyRequestParams(parameter, value, id);

    }


    @Then("I should get status code {string}")
    public void i_should_get_status_code(String string) {
        verifyStatusCode(Integer.parseInt(string));
    }

    @Given("I authorize as")
    public void i_authorize_as() {
        authorize();
    }


}
