package stepDefinitions.uploadSteps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.upload.UploadPage;
import utils.constants.Paths;

public class UploadSteps extends UploadPage {

    @Given("User is on the upload page")
    public void user_is_on_the_upload_page() {
        navigateToHomepage();
    }

    @When("she uploads a valid file {string}")
    public void she_uploads_a_valid_file(String filePath) {
        String absPath = System.getProperty("user.dir") + filePath;
        uploadFile(absPath);
    }

    @When("she submits the file")
    public void she_submits_the_file() {
        submitFile();
    }

    @Then("she should be able to see successful message")
    public void she_should_be_able_to_see_successful_message() {
        verifySuccessSendMessage();
    }


    @When("she uploads an valid file of size more than 200 MB")
    public void she_uploads_an_valid_file_of_size_more_than_mb() {
        uploadFile(Paths.pathToInvalidSizeFile);

    }

    @Then("she should get an error message {string}")
    public void she_should_get_an_error_message(String errorMessage) {
        verifyErrorMessage(errorMessage);
    }
}
