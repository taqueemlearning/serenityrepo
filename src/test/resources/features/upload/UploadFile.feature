Feature: Upload a file
  As an online shopper
  I want to upload a supporting file
  So that I can contact the customer care and provide documents supporting my claim

  Scenario Outline: User should be able to upload a valid file
    Given User is on the upload page
    When she uploads a valid file "<filePath>"
    And she submits the file
    Then she should be able to see successful message
    Examples:
    |filePath|
    |/src/main/resources/testData/uploadingFiles/GOPR0324.JPG|
    |/src/main/resources/testData/uploadingFiles/GOPR0324.pdf|
    |/src/main/resources/testData/uploadingFiles/GOPR0324.zip|

  Scenario: User should not be able to upload a file of size more than 500 MB
    Given User is on the upload page
    When she uploads an valid file of size more than 200 MB
    And she submits the file
    Then she should get an error message "Size not Permitted"