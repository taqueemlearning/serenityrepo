@apiTest
Feature: Booking tickets for events
  As a user of online ticket platform
  I want to book a ticket
  So that I can either get the ticket details or cancel the ticket.


  Scenario: Book a ticket
    Given I am on the ticket platform
    When I enter the ticket details
    |firstName|lastName|totalPrice|depositPaid|additionalneeds|
    Then I should be able to book a ticket

  Scenario: Get the ticket details by ticket ID
    Given I am on the ticket platform
    When I enter the ticket id as "15"
    Then I should see the ticket details

  Scenario: Get the ticket details by invalid ticket ID
    Given I am on the ticket platform
    When I enter the ticket id as "@"
    Then I should get error "404"

  Scenario: Modify the ticket details by ID
    Given I am on the ticket platform
    When I modify the "firstname" as "Smith" for the Booking id "1"
    Then I should get status code "403"

    @auth
  Scenario: Modify the ticket details by ID with admin access
    Given I am on the ticket platform
    And I authorize as
    When I modify the "firstname" as "Smith" for the Booking id "15"
    Then I should get status code "200"