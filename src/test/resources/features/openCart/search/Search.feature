Feature: Search by words
  As an online shopper
  I want to search for different items
  So that I can select my choice and add to cart

  Scenario Outline: Search for alphanumeric should be possible
    Given User is on the open Cart Homepage
    When he looks up "<Item>"
    Then he should see the results about "<Item>"
    Examples:
      | Item    |
      | Iphone  |
      | Samsung |
      | TV      |
      | 10      |

  Scenario Outline: Searching with a special character should not return any result
    Given User is on the open Cart Homepage
    When he looks up "<Item>"
    Then he should not see any products
    Examples:
      | Item |
      | !    |
      | @    |
      | %    |

  Scenario: Clicking on Search button without any input should reload the page
    Given User is on the open Cart Homepage
    When he search without any input
    Then he should see the results